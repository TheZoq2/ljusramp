//! examples/idle.rs

#![no_main]
#![no_std]

use cortex_m_semihosting::{debug, hprintln};
use panic_semihosting as _;
use rtic::app;

use cortex_m::asm;
use cortex_m::singleton;
use cortex_m_rt::entry;

use nb::block; // Only used for slowing down the rainbow effect
use smart_leds::{
    hsv::{hsv2rgb, Hsv},
    RGB8,
};
use stm32f1xx_hal::{
    pac,
    prelude::*,
    spi::{Mode, Phase, Polarity, Spi, SpiPayload, Spi1NoRemap},
    timer::Timer,
    dma::{dma1, TxDma},
    gpio::{Alternate, PushPull, Input, Floating, ExtiPin, Edge},
    gpio::gpioa::{PA5, PA6, PA7},
};
use smart_leds::SmartLedsWrite;
use stm32f1_ws2812::{led_spi_bit_amount, Leds};

const LED_COUNT: usize = 100;
const LED_BIT_COUNT: usize = led_spi_bit_amount(LED_COUNT);

type LedManager = Leds<TxDma<SpiPayload<pac::SPI1, Spi1NoRemap, (PA5<Alternate<PushPull>>, PA6<Input<Floating>>, PA7<Alternate<PushPull>>)>, dma1::C3>>;


#[app(device = stm32f1xx_hal::pac, peripherals = true)]
const APP: () = {
    struct Resources {
        leds: LedManager,
    }

    #[init]
    fn init(ctx: init::Context) -> init::LateResources {
        // Standard f1 setup
        let dp = ctx.device;
        let cp = ctx.core;

        let mut rcc = dp.RCC.constrain();
        let mut flash = dp.FLASH.constrain();
        let clocks = rcc.cfgr.sysclk(16_000_000.hz()).freeze(&mut flash.acr);
        let mut afio = dp.AFIO.constrain(&mut rcc.apb2);
        let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);
        let mut exti = dp.EXTI;

        // For a slower rainbow, we'll use a timer
        let mut timer = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1).start_count_down(60.hz());

        // Set up the SPI peripheral
        let pins = (
            gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl),
            gpioa.pa6.into_floating_input(&mut gpioa.crl),
            gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl),
        );

        let spi_mode = Mode {
            polarity: Polarity::IdleLow,
            phase: Phase::CaptureOnFirstTransition,
        };
        let spi = Spi::spi1(
            dp.SPI1,
            pins,
            &mut afio.mapr,
            spi_mode,
            3.mhz(),
            clocks,
            &mut rcc.apb2,
        );

        // Set up the DMA channels
        let dma_channels = dp.DMA1.split(&mut rcc.ahb);
        let spi_dma = spi.with_tx_dma(dma_channels.3);



        let data_buffer: &'static mut [u8; LED_BIT_COUNT] =
            singleton!(: [u8; LED_BIT_COUNT] = [0; LED_BIT_COUNT]).unwrap();

        let mut leds = Leds::new(data_buffer, spi_dma);

        let mut button = gpioa.pa0.into_pull_down_input(&mut gpioa.crl);
        button.make_interrupt_source(&mut afio);
        button.trigger_on_edge(&mut exti, Edge::RISING_FALLING);
        button.enable_interrupt(&mut exti);

        rtic::pend(pac::Interrupt::USART1);
        rtic::pend(pac::Interrupt::USB_LP_CAN_RX0);

        init::LateResources {
            leds
        }
    }

    #[idle(resources=[leds])]
    fn idle(ctx: idle::Context) -> ! {
        // Holds the colour values
        let mut current_color = [
            Hsv{hue: 0, sat: 255, val: 255},
            Hsv{hue: 120, sat: 255, val: 255},
            Hsv{hue: 0, sat: 0, val: 255},
            Hsv{hue: 0, sat: 0, val: 255},
        ];

        let leds = ctx.resources.leds;
        loop {
            let rgb_iterator = current_color.iter().cloned().map(hsv2rgb);

            leds.write(rgb_iterator);
        }
    }

    #[task(binds = USART1)]
    fn foo(c: foo::Context) {
        asm::bkpt();
        asm::bkpt();
        asm::bkpt();
        asm::bkpt();
        asm::bkpt();
        asm::bkpt();
    }

    #[task(binds = USB_LP_CAN_RX0)]
    fn usbtest(_: usbtest::Context) {
        asm::bkpt();
    }
};
